﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using task13.DTOs.Director;
using task13.Models;

namespace task13.Profiles
{
    public class DirectorProfile : Profile
    {
        public DirectorProfile()
        {
            CreateMap<Director, DirectorDTO>().ReverseMap();
            CreateMap<Director, DirectorNameDTO>().ReverseMap();
        }
    }
}
