﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using task13.DTOs.Franchise;
using task13.Models;

namespace task13.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            CreateMap<Franchise, FranchiseDTO>().ReverseMap();
            CreateMap<Franchise, FranchiseWithMoviesDTO>().ReverseMap();
        }
    }
}
