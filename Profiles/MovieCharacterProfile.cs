﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using task13.DTOs.Actor;
using task13.DTOs.Character;
using task13.DTOs.Movie;
using task13.DTOs.MovieCharacter;
using task13.Models;

namespace task13.Profiles
{
    public class MovieCharacterProfile : Profile
    {
        public MovieCharacterProfile()
        {
            CreateMap<MovieCharacter, MovieCharacterDTO>().ReverseMap();
            CreateMap<MovieCharacterDTO, ActorDTO>().ReverseMap();
            CreateMap<MovieCharacterDTO, CharacterDTO>().ReverseMap();
            CreateMap<MovieCharacterDTO, MovieDTO>().ReverseMap();
            CreateMap<MovieCharacter, MovieCharacterSimplifiedDTO>().
                ForMember(mcsdto => mcsdto.ActorName, opt => opt.MapFrom(mc => new string(mc.Actor.FirstName + " " + mc.Actor.LastName))).
                ForMember(mcsdto => mcsdto.CharacterName, opt => opt.MapFrom(mc => mc.Character.FullName)).
                ForMember(mcsdto => mcsdto.MovieTitle, opt => opt.MapFrom(mc => mc.Movie.Title));
            CreateMap<MovieCharacter, MovieCharacterIdDTO>().ReverseMap();
        }
    }
}
