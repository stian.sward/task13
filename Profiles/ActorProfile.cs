﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using task13.Models;
using task13.DTOs.Actor;

namespace task13.Profiles
{
    public class ActorProfile : Profile
    {
        public ActorProfile()
        {
            CreateMap<Actor, ActorDTO>().ReverseMap();
            CreateMap<Actor, ActorNameDTO>().ReverseMap();
            CreateMap<MovieCharacter, ActorRoleDTO>().
                ForMember(ardto => ardto.FirstName, opt => opt.MapFrom(mc => mc.Actor.FirstName)).
                ForMember(ardto => ardto.LastName, opt => opt.MapFrom(mc => mc.Actor.LastName)).
                ForMember(ardto => ardto.RoleName, opt => opt.MapFrom(mc => mc.Character.FullName)).
                ForMember(ardto => ardto.RoleAlias, opt => opt.MapFrom(mc => mc.Character.Alias)).
                ForMember(ardto => ardto.Movie, opt => opt.MapFrom(mc => mc.Movie.Title));
            CreateMap<MovieCharacter, ActorDTO>().
                ForMember(adto => adto.FirstName, opt => opt.MapFrom(mc => mc.Actor.FirstName)).
                ForMember(adto => adto.LastName, opt => opt.MapFrom(mc => mc.Actor.LastName)).
                ForMember(adto => adto.MiddleNames, opt => opt.MapFrom(mc => mc.Actor.MiddleNames)).
                ForMember(adto => adto.Gender, opt => opt.MapFrom(mc => mc.Actor.Gender)).
                ForMember(adto => adto.DOB, opt => opt.MapFrom(mc => mc.Actor.DOB)).
                ForMember(adto => adto.PlaceOfBirth, opt => opt.MapFrom(mc => mc.Actor.PlaceOfBirth)).
                ForMember(adto => adto.Biography, opt => opt.MapFrom(mc => mc.Actor.Biography)).
                ForMember(adto => adto.PicURL, opt => opt.MapFrom(mc => mc.Actor.PicURL));
        }
    }
}
