﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using task13.DTOs.Actor;
using task13.DTOs.Character;
using task13.Models;

namespace task13.Profiles
{
    public class CharacterProfile : Profile
    {
        public CharacterProfile()
        {
            CreateMap<Character, CharacterDTO>().ReverseMap();
            CreateMap<MovieCharacter, CharacterWithActorDTO>().
                ForMember(cwadto => cwadto.FullName, opt => opt.MapFrom(mc => mc.Character.FullName)).
                ForMember(cwadto => cwadto.Alias, opt => opt.MapFrom(mc => mc.Character.Alias)).
                ForMember(cwadto => cwadto.Gender, opt => opt.MapFrom(mc => mc.Character.Gender)).
                ForMember(cwadto => cwadto.PicURL, opt => opt.MapFrom(mc => mc.Character.PicURL)).
                ForMember(cwadto => cwadto.Actor, opt => opt.MapFrom(mc => mc.Actor));
        }
    }
}
