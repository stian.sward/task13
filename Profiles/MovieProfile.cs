﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using task13.DTOs.Director;
using task13.DTOs.Franchise;
using task13.DTOs.Movie;
using task13.Models;

namespace task13.Profiles
{
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {
            CreateMap<Movie, MovieDTO>().ReverseMap();
            CreateMap<MovieDTO, DirectorDTO>().ReverseMap();
            CreateMap<MovieDTO, FranchiseDTO>().ReverseMap();
            CreateMap<Movie, MovieSimplifiedDTO>().ReverseMap();
        }
    }
}
