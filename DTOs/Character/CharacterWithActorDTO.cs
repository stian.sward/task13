﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using task13.DTOs.Actor;

namespace task13.DTOs.Character
{
    public class CharacterWithActorDTO
    {
        public string FullName { get; set; }
        public string Alias { get; set; }
        public string Gender { get; set; }
        public string PicURL { get; set; }
        public ActorDTO Actor { get; set; }
    }
}
