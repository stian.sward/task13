﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace task13.DTOs.Movie
{
    public class MovieSimplifiedDTO
    {
        public string Title { get; set; }
        public DateTime ReleaseYear { get; set; }
        public string Genre { get; set; }
        public string Synopsis { get; set; }
        public int DirectorId { get; set; }
        public int FranchiseId { get; set; }
        public string PicURL { get; set; }
        public string TrailerURL { get; set; }
    }
}
