﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using task13.DTOs.Director;
using task13.DTOs.Franchise;

namespace task13.DTOs.Movie
{
    public class MovieDTO
    {
        public string Title { get; set; }
        public string Genre { get; set; }
        public string Synopsis { get; set; }
        public DateTime? ReleaseYear { get; set; }
        public DirectorDTO Director { get; set; }
        public FranchiseDTO Franchise { get; set; }
        public string PicURL { get; set; }
        public string TrailerURL { get; set; }
    }
}
