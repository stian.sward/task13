﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using task13.DTOs.Actor;
using task13.DTOs.Movie;
using task13.DTOs.Character;

namespace task13.DTOs.MovieCharacter
{
    public class MovieCharacterDTO
    {
        public ActorDTO Actor { get; set; }
        public CharacterDTO Character { get; set; }
        public MovieDTO Movie { get; set; }
        public string PicURL { get; set; }
    }
}
