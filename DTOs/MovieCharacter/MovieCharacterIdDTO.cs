﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace task13.DTOs.MovieCharacter
{
    public class MovieCharacterIdDTO
    {
        public int ActorId { get; set; }
        public int CharacterId { get; set; }
        public int MovieId { get; set; }
        public string PicURL { get; set; }
    }
}
