﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace task13.DTOs.MovieCharacter
{
    public class MovieCharacterSimplifiedDTO
    {
        public string ActorName { get; set; }
        public string CharacterName { get; set; }
        public string MovieTitle { get; set; }
    }
}
