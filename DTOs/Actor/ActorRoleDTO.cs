﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace task13.DTOs.Actor
{
    public class ActorRoleDTO
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string RoleName { get; set; }
        public string RoleAlias { get; set; }
        public string Movie { get; set; }
    }
}
