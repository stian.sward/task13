﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace task13.Migrations
{
    public partial class InitialDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Actors",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    MiddleNames = table.Column<string>(nullable: true),
                    Gender = table.Column<string>(nullable: true),
                    DOB = table.Column<DateTime>(nullable: true),
                    PlaceOfBirth = table.Column<string>(nullable: true),
                    Biography = table.Column<string>(nullable: true),
                    PicURL = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Actors", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Characters",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FullName = table.Column<string>(nullable: true),
                    Alias = table.Column<string>(nullable: true),
                    Gender = table.Column<string>(nullable: true),
                    PicURL = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Characters", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Directors",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    MiddleNames = table.Column<string>(nullable: true),
                    Gender = table.Column<string>(nullable: true),
                    DOB = table.Column<DateTime>(nullable: true),
                    PlaceOfBirth = table.Column<string>(nullable: true),
                    Biography = table.Column<string>(nullable: true),
                    PicURL = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Directors", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Franchises",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Franchises", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Movies",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(nullable: true),
                    Genre = table.Column<string>(nullable: true),
                    Synopsis = table.Column<string>(nullable: true),
                    ReleaseYear = table.Column<DateTime>(nullable: true),
                    PicURL = table.Column<string>(nullable: true),
                    TrailerURL = table.Column<string>(nullable: true),
                    DirectorId = table.Column<int>(nullable: true),
                    FranchiseId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Movies", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Movies_Directors_DirectorId",
                        column: x => x.DirectorId,
                        principalTable: "Directors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Movies_Franchises_FranchiseId",
                        column: x => x.FranchiseId,
                        principalTable: "Franchises",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MovieCharacters",
                columns: table => new
                {
                    ActorId = table.Column<int>(nullable: false),
                    CharacterId = table.Column<int>(nullable: false),
                    MovieId = table.Column<int>(nullable: false),
                    PicURL = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MovieCharacters", x => new { x.ActorId, x.CharacterId, x.MovieId });
                    table.ForeignKey(
                        name: "FK_MovieCharacters_Actors_ActorId",
                        column: x => x.ActorId,
                        principalTable: "Actors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MovieCharacters_Characters_CharacterId",
                        column: x => x.CharacterId,
                        principalTable: "Characters",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MovieCharacters_Movies_MovieId",
                        column: x => x.MovieId,
                        principalTable: "Movies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Actors",
                columns: new[] { "Id", "Biography", "DOB", "FirstName", "Gender", "LastName", "MiddleNames", "PicURL", "PlaceOfBirth" },
                values: new object[,]
                {
                    { 1, null, new DateTime(1989, 6, 23, 0, 0, 0, 0, DateTimeKind.Unspecified), "Daniel", "Male", "Radcliffe", null, "https://m.media-amazon.com/images/M/MV5BMTg4NTExODc3Nl5BMl5BanBnXkFtZTgwODUyMDEzMDE@._V1_.jpg", "Fulham, London, England, UK" },
                    { 16, null, new DateTime(1944, 5, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), "John", "Male", "Rhys-Davies", null, "https://m.media-amazon.com/images/M/MV5BMjMwNDY3NjQxMF5BMl5BanBnXkFtZTcwMDc3NTYyOQ@@._V1_SY1000_CR0,0,837,1000_AL_.jpg", "Ammanford, Wales, UK" },
                    { 15, null, new DateTime(1976, 12, 8, 0, 0, 0, 0, DateTimeKind.Unspecified), "Dominic", "Male", "Monaghan", null, "https://m.media-amazon.com/images/M/MV5BMTg0MTc2ODIwNl5BMl5BanBnXkFtZTcwMjQ4MjEwMw@@._V1_.jpg", "Berlin, Germany" },
                    { 14, null, new DateTime(1968, 8, 28, 0, 0, 0, 0, DateTimeKind.Unspecified), "Billy", "Male", "Boyd", null, "https://m.media-amazon.com/images/M/MV5BMTQzOTEyMTQ3OF5BMl5BanBnXkFtZTYwNzkzMDk1._V1_.jpg", "Glasgow, Scotland, UK" },
                    { 13, null, new DateTime(1977, 1, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), "Orlando", "Male", "Bloom", "Jonathan Blanchard", "https://m.media-amazon.com/images/M/MV5BMjE1MDkxMjQ3NV5BMl5BanBnXkFtZTcwMzQ3Mjc4MQ@@._V1_.jpg", "Canterbury, Kent, England, UK" },
                    { 11, null, new DateTime(1977, 7, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Liv", "Female", "Tyler", null, "https://m.media-amazon.com/images/M/MV5BMTY4NjQxMjc5MF5BMl5BanBnXkFtZTcwMzg5Mzg4Ng@@._V1_SY1000_CR0,0,666,1000_AL_.jpg", "New York City, New York, USA" },
                    { 10, null, new DateTime(1958, 10, 20, 0, 0, 0, 0, DateTimeKind.Unspecified), "Viggo", "Male", "Mortensen", null, "https://m.media-amazon.com/images/M/MV5BNDQzOTg4NzA2Nl5BMl5BanBnXkFtZTcwMzkwNjkxMg@@._V1_.jpg", "Manhattan, New York City, New York, USA" },
                    { 9, null, new DateTime(1939, 5, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), "Ian", "Male", "McKellen", null, "https://m.media-amazon.com/images/M/MV5BMTQ2MjgyNjk3MV5BMl5BanBnXkFtZTcwNTA3NTY5Mg@@._V1_.jpg", "Burnley, Lancashire, England, UK" },
                    { 12, null, new DateTime(1959, 4, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), "Sean", "Male", "Bean", null, "https://m.media-amazon.com/images/M/MV5BMTkzMzc4MDk5OF5BMl5BanBnXkFtZTcwODg3MjUxNw@@._V1_SY1000_CR0,0,726,1000_AL_.jpg", "Sheffield, South Yorkshire, England, UK" },
                    { 7, null, new DateTime(1962, 12, 22, 0, 0, 0, 0, DateTimeKind.Unspecified), "Ralph", "Male", "Fiennes", null, "https://m.media-amazon.com/images/M/MV5BMzc5MjE1NDgyN15BMl5BanBnXkFtZTcwNzg2ODgwNA@@._V1_.jpg", "Ipswich, Suffolk, England, UK" },
                    { 6, null, new DateTime(1953, 1, 27, 0, 0, 0, 0, DateTimeKind.Unspecified), "Richard", "Male", "Bremmer", null, "https://m.media-amazon.com/images/M/MV5BMjA2NTA4NzIyM15BMl5BanBnXkFtZTcwNTY5MzAwOA@@._V1_.jpg", "Warwickshire, England, UK" },
                    { 5, null, new DateTime(1940, 10, 19, 0, 0, 0, 0, DateTimeKind.Unspecified), "Michael", "Male", "Gambon", null, "https://m.media-amazon.com/images/M/MV5BMTY3OTc4MTgyN15BMl5BanBnXkFtZTcwNTAxNjA3Mg@@._V1_.jpg", "Cabra, Dublin, Ireland" },
                    { 4, null, new DateTime(1930, 10, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Richard", "Male", "Harris", null, "https://m.media-amazon.com/images/M/MV5BMTgzNTA5ODg1NV5BMl5BanBnXkFtZTcwMDU3MTU5Mw@@._V1_.jpg", "Limerick, Ireland" },
                    { 3, null, new DateTime(1990, 4, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), "Emma", "Female", "Watson", null, "https://m.media-amazon.com/images/M/MV5BMTQ3ODE2NTMxMV5BMl5BanBnXkFtZTgwOTIzOTQzMjE@._V1_SY1000_CR0,0,810,1000_AL_.jpg", "Paris, France" },
                    { 2, null, new DateTime(1988, 8, 24, 0, 0, 0, 0, DateTimeKind.Unspecified), "Rupert", "Male", "Grint", null, "https://m.media-amazon.com/images/M/MV5BMjI3MDA3NjA1N15BMl5BanBnXkFtZTcwMDcyMDYzNw@@._V1_.jpg", "Stevenage, Hertfordshire, England, UK" },
                    { 8, null, new DateTime(1981, 1, 28, 0, 0, 0, 0, DateTimeKind.Unspecified), "Elijah", "Male", "Wood", null, "https://m.media-amazon.com/images/M/MV5BMTM0NDIxMzQ5OF5BMl5BanBnXkFtZTcwNzAyNTA4Nw@@._V1_SY1000_CR0,0,666,1000_AL_.jpg", "Cedar Rapis, Iowa, USA" }
                });

            migrationBuilder.InsertData(
                table: "Characters",
                columns: new[] { "Id", "Alias", "FullName", "Gender", "PicURL" },
                values: new object[,]
                {
                    { 9, "Undómiel", "Arwen", "Female", "https://m.media-amazon.com/images/M/MV5BNzdlMWUxYTktYTJlYi00M2RkLWJlNzAtMzM5YzUwNmE1MzlmXkEyXkFqcGdeQXVyMzQ3Nzk5MTU@._V1_.jpg" },
                    { 14, "Elf-friend", "Gimli", "Male", "https://m.media-amazon.com/images/M/MV5BMTg2NDYxMzQ0Ml5BMl5BanBnXkFtZTcwNzExMjk2Mw@@._V1_SY1000_CR0,0,1497,1000_AL_.jpg" },
                    { 13, "Merry", "Meriadoc Brandybuck", "Male", "https://m.media-amazon.com/images/M/MV5BMTI4ODY2NjMyNF5BMl5BanBnXkFtZTcwMjkwMjk2Mw@@._V1_SY1000_CR0,0,1479,1000_AL_.jpg" },
                    { 12, "Pippin", "Peregrin Took", "Male", "https://m.media-amazon.com/images/M/MV5BNDQ4MThiZjEtNDdhMC00NTU4LWIxYzItMDgzMDVlZjlhYzk0XkEyXkFqcGdeQXVyMzQ3Nzk5MTU@._V1_.jpg" },
                    { 11, "Greenleaf", "Legolas", "Male", "https://m.media-amazon.com/images/M/MV5BMDM5YzJlNmItZWJiZS00Y2ZkLTkyZGUtZjllMDMxMDk3OWMzXkEyXkFqcGdeQXVyMzQ3Nzk5MTU@._V1_.jpg" },
                    { 10, null, "Boromir", "Male", "https://m.media-amazon.com/images/M/MV5BNzRjODZjMzktMTY4NS00ZWIzLWIxYTgtMTYzZWMzYjZhODkwXkEyXkFqcGdeQXVyNzU3Nzk4MDQ@._V1_.jpg" },
                    { 8, "Strider", "Aragorn", "Male", "https://m.media-amazon.com/images/M/MV5BNzc4NzYyNDk0MV5BMl5BanBnXkFtZTcwMTExMjk2Mw@@._V1_SY1000_CR0,0,736,1000_AL_.jpg" },
                    { 3, "Hermione", "Hermione Granger", "Female", "https://m.media-amazon.com/images/M/MV5BN2ZmNmRlNDQtM2M4Zi00N2M3LWEwNmEtOTE0ZGY3OTZlYjFiXkEyXkFqcGdeQXVyNjQ4ODE4MzQ@._V1_SY1000_CR0,0,663,1000_AL_.jpg" },
                    { 6, "Sneaky hobbit", "Frodo Baggins", "Male", "https://m.media-amazon.com/images/M/MV5BMTYxNjY1ODQ3MF5BMl5BanBnXkFtZTcwMjgwMjk2Mw@@._V1_SY1000_CR0,0,1489,1000_AL_.jpg" },
                    { 5, "Lord Voldemort", "Tom Marvolo Riddle", "Male", "https://m.media-amazon.com/images/M/MV5BMTY4ODgzNjEwOV5BMl5BanBnXkFtZTcwMTg3MDI5Mw@@._V1_SY1000_CR0,0,1678,1000_AL_.jpg" },
                    { 4, "Professor Dumbledore", "Albus Percival Wulfric Brian Dumbledore", "Male", "https://m.media-amazon.com/images/M/MV5BZTgzOTNhNzItNWNlYi00YzdhLWE4ODktMDhlYWYxYzZmZDU3XkEyXkFqcGdeQXVyMjI3NzE4MTM@._V1_.jpg" },
                    { 2, "Ron", "Ronald Weasley", "Male", "https://m.media-amazon.com/images/M/MV5BMTkyMDM1MTQzOF5BMl5BanBnXkFtZTYwMzg0NTg3._V1_.jpg" },
                    { 1, "Harry", "Harry Potter", "Male", "https://www.irishtimes.com/polopoly_fs/1.3170107.1501253408!/image/image.jpg_gen/derivatives/ratio_1x1_w1200/image.jpg" },
                    { 7, "Gandalf the Grey, Gandalf the White", "Gandalf", "Male", "https://m.media-amazon.com/images/M/MV5BMTY1OTE1NDY4NV5BMl5BanBnXkFtZTcwMzkwMjk2Mw@@._V1_SY1000_CR0,0,1501,1000_AL_.jpg" }
                });

            migrationBuilder.InsertData(
                table: "Directors",
                columns: new[] { "Id", "Biography", "DOB", "FirstName", "Gender", "LastName", "MiddleNames", "PicURL", "PlaceOfBirth" },
                values: new object[,]
                {
                    { 1, null, new DateTime(1958, 9, 10, 0, 0, 0, 0, DateTimeKind.Unspecified), "Chris", "Male", "Columbus", null, "https://m.media-amazon.com/images/M/MV5BMTY2MTYzNzUyNl5BMl5BanBnXkFtZTYwMDI0NzA0._V1_.jpg", "Spangler, Pennsylvania, USA" },
                    { 2, null, new DateTime(1961, 11, 28, 0, 0, 0, 0, DateTimeKind.Unspecified), "Alfonso", "Male", "Cuarón", null, "https://m.media-amazon.com/images/M/MV5BMjA0ODY4OTk4Nl5BMl5BanBnXkFtZTcwNTkxMzYyMg@@._V1_.jpg", "Mexico City, Distrito Federal, Mexico" },
                    { 3, null, new DateTime(1942, 3, 28, 0, 0, 0, 0, DateTimeKind.Unspecified), "Mike", "Male", "Newell", null, "https://m.media-amazon.com/images/M/MV5BMTgzNDI1ODc4N15BMl5BanBnXkFtZTYwNjg3NTc1._V1_.jpg", "St. Albans, Hertfordshire, England, UK" },
                    { 4, null, new DateTime(1963, 10, 8, 0, 0, 0, 0, DateTimeKind.Unspecified), "David", "Male", "Yates", null, "https://m.media-amazon.com/images/M/MV5BMTY2NTU4NjY4M15BMl5BanBnXkFtZTYwNjIxOTI1._V1_.jpg", "St. Helens, Merseyside, England, UK" },
                    { 5, null, new DateTime(1961, 10, 31, 0, 0, 0, 0, DateTimeKind.Unspecified), "Peter", "Male", "Jackson", null, "https://m.media-amazon.com/images/M/MV5BMTY1MzQ3NjA2OV5BMl5BanBnXkFtZTcwNTExOTA5OA@@._V1_SY1000_CR0,0,725,1000_AL_.jpg", "Pukerua Bay, North Island, New Zealand" }
                });

            migrationBuilder.InsertData(
                table: "Franchises",
                columns: new[] { "Id", "Description", "Name" },
                values: new object[,]
                {
                    { 1, "The Wizarding World of Harry Potter", "Harry Potter" },
                    { 2, "Peter Jackson's trilogy based on J. R. R. Tolkien's books", "The Lord of the Rings" }
                });

            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "Id", "DirectorId", "FranchiseId", "Genre", "PicURL", "ReleaseYear", "Synopsis", "Title", "TrailerURL" },
                values: new object[,]
                {
                    { 1, 1, 1, "Adventure, Family, Fantasy", "https://i.ibb.co/mHMF0jr/1-HP-Philosopher.png", new DateTime(2001, 11, 16, 0, 0, 0, 0, DateTimeKind.Unspecified), "An orphaned boy enrolls in a school of wizardry, where he learns the truth about himself, his family and the terrible evil that haunts the magical world.", "Harry Potter and the Philosopher's Stone", "https://www.youtube.com/watch?v=VyHV0BRtdxo" },
                    { 2, 1, 1, "Adventure, Family, Fantasy", "https://i.ibb.co/QmvWWqb/2-HP-Chamber.png", new DateTime(2002, 11, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), "An ancient prophecy seems to be coming true when a mysterious presence begins stalking the corridors of a school of magic and leaving its victims paralyzed.", "Harry Potter and the Chamber of Secrets", "https://www.youtube.com/watch?v=1bq0qff4iF8" },
                    { 3, 2, 1, "Adventure, Family, Fantasy", "https://i.ibb.co/2dPm1yk/3-HP-Prisoner.png", new DateTime(2004, 5, 31, 0, 0, 0, 0, DateTimeKind.Unspecified), "Harry Potter, Ron and Hermione return to Hogwarts School of Witchcraft and Wizardry for their third year of study, where they delve into the mystery surrounding an escaped prisoner who poses a dangerous threat to the young wizard.", "Harry Potter and the Prisoner of Azkaban", "https://www.youtube.com/watch?v=1ZdlAg3j8nI" },
                    { 4, 3, 1, "Adventure, Family, Fantasy", "https://i.ibb.co/my6NLsn/4-HP-Goblet.png", new DateTime(2005, 11, 18, 0, 0, 0, 0, DateTimeKind.Unspecified), "Harry Potter finds himself competing in a hazardous tournament between rival schools of magic, but he is distracted by recurring nightmares.", "Harry Potter and the Goblet of Fire", "https://www.youtube.com/watch?v=3EGojp4Hh6I" },
                    { 5, 4, 1, "Action, Adventure, Family", "https://i.ibb.co/GPtWH4w/5-HP-Order.png", new DateTime(2007, 7, 12, 0, 0, 0, 0, DateTimeKind.Unspecified), "With their warning about Lord Voldemort's return scoffed at, Harry and Dumbledore are targeted by the Wizard authorities as an authoritarian bureaucrat slowly seizes power at Hogwarts.", "Harry Potter and the Order of the Phoenix", "https://www.youtube.com/watch?v=y6ZW7KXaXYk" },
                    { 6, 4, 1, "Action, Adventure, Family", "https://i.ibb.co/PwKZB23/6-HP-Half-Blood.png", new DateTime(2009, 7, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), "As Harry Potter begins his sixth year at Hogwarts, he discovers an old book marked as 'the property of the Half - Blood Prince' and begins to learn more about Lord Voldemort's dark past.", "Harry Potter and the Half-Blood Prince", "https://www.youtube.com/watch?v=sg81Lts5kYY" },
                    { 7, 4, 1, "Adventure, Family, Fantasy", "https://i.ibb.co/dk2SxZb/7-HP-Deathly-P1.png", new DateTime(2010, 11, 19, 0, 0, 0, 0, DateTimeKind.Unspecified), "As Harry, Ron, and Hermione race against time and evil to destroy the Horcruxes, they uncover the existence of the three most powerful objects in the wizarding world: the Deathly Hallows.", "Harry Potter and the Deathly Hallows: Part 1", "https://www.youtube.com/watch?v=MxqsmsA8y5k" },
                    { 8, 4, 1, "Adventure, Drama, Fantasy", "https://i.ibb.co/VjDwsf8/8-HP-Deathly-P2.png", new DateTime(2011, 7, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), "Harry, Ron, and Hermione search for Voldemort's remaining Horcruxes in their effort to destroy the Dark Lord as the final battle rages on at Hogwarts.", "Harry Potter and the Deathly Hallows: Part 2", "https://www.youtube.com/watch?v=mObK5XD8udk" },
                    { 9, 5, 2, "Action, Adventure, Drama", "https://m.media-amazon.com/images/M/MV5BN2EyZjM3NzUtNWUzMi00MTgxLWI0NTctMzY4M2VlOTdjZWRiXkEyXkFqcGdeQXVyNDUzOTQ5MjY@._V1_SY999_CR0,0,673,999_AL_.jpg", new DateTime(2001, 12, 19, 0, 0, 0, 0, DateTimeKind.Unspecified), "A meek Hobbit from the Shire and eight companions set out on a journey to destroy the powerful One Ring and save Middle-earth from the Dark Lord Sauron.", "The Lord of the Rings: The Fellowship of the Ring", "https://www.youtube.com/watch?v=V75dMMIW2B4" },
                    { 10, 5, 2, "Action, Adventure, Drama", "https://m.media-amazon.com/images/M/MV5BZGMxZTdjZmYtMmE2Ni00ZTdkLWI5NTgtNjlmMjBiNzU2MmI5XkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_SY1000_CR0,0,642,1000_AL_.jpg", new DateTime(2002, 12, 18, 0, 0, 0, 0, DateTimeKind.Unspecified), "While Frodo and Sam edge closer to Mordor with the help of the shifty Gollum, the divided fellowship makes a stand against Sauron's new ally, Saruman, and his hordes of Isengard.", "The Lord of the Rings: The Two Towers", "https://www.youtube.com/watch?v=LbfMDwc4azU" },
                    { 11, 5, 2, "Action, Adventure, Drama", "https://m.media-amazon.com/images/M/MV5BNzA5ZDNlZWMtM2NhNS00NDJjLTk4NDItYTRmY2EwMWZlMTY3XkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_SY1000_CR0,0,675,1000_AL_.jpg", new DateTime(2003, 12, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), "Gandalf and Aragorn lead the World of Men against Sauron's army to draw his gaze from Frodo and Sam as they approach Mount Doom with the One Ring.", "The Lord of the Rings: The Return of the King", "https://www.youtube.com/watch?v=y2rYRu8UW8M" }
                });

            migrationBuilder.InsertData(
                table: "MovieCharacters",
                columns: new[] { "ActorId", "CharacterId", "MovieId", "PicURL" },
                values: new object[,]
                {
                    { 1, 1, 1, "https://m.media-amazon.com/images/M/MV5BNTI0MTQzODYzM15BMl5BanBnXkFtZTYwNTc0NTg3._V1_.jpg" },
                    { 2, 2, 8, null },
                    { 3, 3, 8, null },
                    { 5, 4, 8, null },
                    { 7, 5, 8, null },
                    { 8, 6, 9, null },
                    { 9, 7, 9, null },
                    { 10, 8, 9, null },
                    { 11, 9, 9, null },
                    { 12, 10, 9, null },
                    { 13, 11, 9, null },
                    { 14, 12, 9, null },
                    { 15, 13, 9, null },
                    { 16, 14, 9, null },
                    { 8, 6, 10, null },
                    { 9, 7, 10, null },
                    { 10, 8, 10, null },
                    { 11, 9, 10, null },
                    { 13, 11, 10, null },
                    { 14, 12, 10, null },
                    { 15, 13, 10, null },
                    { 16, 14, 10, null },
                    { 8, 6, 11, null },
                    { 9, 7, 11, null },
                    { 10, 8, 11, null },
                    { 11, 9, 11, null },
                    { 13, 11, 11, null },
                    { 14, 12, 11, null },
                    { 1, 1, 8, null },
                    { 7, 5, 7, null },
                    { 5, 4, 7, null },
                    { 3, 3, 7, null },
                    { 2, 2, 1, null },
                    { 3, 3, 1, null },
                    { 4, 4, 1, null },
                    { 6, 5, 1, null },
                    { 1, 1, 2, null },
                    { 2, 2, 2, null },
                    { 3, 3, 2, null },
                    { 4, 4, 2, null },
                    { 1, 1, 3, null },
                    { 2, 2, 3, null },
                    { 3, 3, 3, null },
                    { 5, 4, 3, null },
                    { 1, 1, 4, null },
                    { 15, 13, 11, null },
                    { 2, 2, 4, null },
                    { 5, 4, 4, null },
                    { 7, 5, 4, null },
                    { 1, 1, 5, null },
                    { 2, 2, 5, null },
                    { 3, 3, 5, null },
                    { 5, 4, 5, null },
                    { 7, 5, 5, null },
                    { 1, 1, 6, null },
                    { 2, 2, 6, null },
                    { 3, 3, 6, null },
                    { 5, 4, 6, null },
                    { 1, 1, 7, null },
                    { 2, 2, 7, null },
                    { 3, 3, 4, null },
                    { 16, 14, 11, null }
                });

            migrationBuilder.CreateIndex(
                name: "IX_MovieCharacters_CharacterId",
                table: "MovieCharacters",
                column: "CharacterId");

            migrationBuilder.CreateIndex(
                name: "IX_MovieCharacters_MovieId",
                table: "MovieCharacters",
                column: "MovieId");

            migrationBuilder.CreateIndex(
                name: "IX_Movies_DirectorId",
                table: "Movies",
                column: "DirectorId");

            migrationBuilder.CreateIndex(
                name: "IX_Movies_FranchiseId",
                table: "Movies",
                column: "FranchiseId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MovieCharacters");

            migrationBuilder.DropTable(
                name: "Actors");

            migrationBuilder.DropTable(
                name: "Characters");

            migrationBuilder.DropTable(
                name: "Movies");

            migrationBuilder.DropTable(
                name: "Directors");

            migrationBuilder.DropTable(
                name: "Franchises");
        }
    }
}
