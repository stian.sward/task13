﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using task13.DTOs.MovieCharacter;
using task13.Models;

namespace task13.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class MovieCharactersController : ControllerBase
    {
        private readonly MovieDbContext _context;
        private readonly IMapper _mapper;

        public MovieCharactersController(MovieDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/v1/MovieCharacters
        /// <summary>
        /// Lists every entry in the MovieCharacters table
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieCharacterDTO>>> GetMovieCharacters()
        {
            var mc = await _context.MovieCharacters.
                Include(mc => mc.Actor).
                Include(mc => mc.Character).
                Include(mc => mc.Movie).ToListAsync();
            if (mc.Count == 0)
            {
                return NotFound();
            }
            return _mapper.Map<IEnumerable<MovieCharacter>, List<MovieCharacterDTO>>(mc);
        }

        // GET: api/v1/MovieCharacters/byIDs
        /// <summary>
        /// Finds the single entry that matches all three parameters
        /// </summary>
        /// <param name="mcidto">A MovieCharacterIdDTO object describing the ID's of the wanted MovieCharacter</param>
        /// <returns>MovieCharacter object that matches all ID's</returns>
        [HttpGet("byID")]
        public async Task<ActionResult<MovieCharacterDTO>> GetMovieCharacterByID(MovieCharacterIdDTO mcidto)
        {
            MovieCharacter movieCharacter = await _context.MovieCharacters.
                Where(mc => mc.ActorId == mcidto.ActorId && mc.CharacterId == mcidto.CharacterId && mc.MovieId == mcidto.MovieId).
                Include(mc => mc.Actor).Include(mc => mc.Character).Include(mc => mc.Movie).SingleOrDefaultAsync();
            if (movieCharacter == null)
            {
                return NotFound();
            }
            return _mapper.Map<MovieCharacterDTO>(movieCharacter);
        }

        // GET: api/v1/MovieCharacters/byactor/5
        /// <summary>
        /// Lists all MovieCharacter objects that a given Actor has been part of
        /// </summary>
        /// <param name="id">ActorID</param>
        /// <returns></returns>
        [HttpGet("byactor/{id}")]
        public async Task<ActionResult<IEnumerable<MovieCharacterDTO>>> GetMovieCharactersByActor(int id)
        {
            var movieCharacters = await _context.MovieCharacters.
                Include(mc => mc.Actor).
                Include(mc => mc.Character).
                Include(mc => mc.Movie).
                Where(mc => mc.ActorId == id).ToListAsync();

            if (movieCharacters.Count == 0)
            {
                return NotFound();
            }

            return _mapper.Map<List<MovieCharacterDTO>>(movieCharacters);
        }

        // GET: api/v1/MovieCharacters/bycharacter/5
        /// <summary>
        /// Lists all MovieCharacter objects that portray a given Character
        /// </summary>
        /// <param name="id">CharacterID</param>
        /// <returns></returns>
        [HttpGet("bycharacter/{id}")]
        public async Task<ActionResult<IEnumerable<MovieCharacterDTO>>> GetMovieCharactersByCharacter(int id)
        {
            var movieCharacters = await _context.MovieCharacters.
                Include(mc => mc.Actor).
                Include(mc => mc.Character).
                Include(mc => mc.Movie).
                Where(mc => mc.CharacterId == id).ToListAsync();

            if (movieCharacters.Count == 0)
            {
                return NotFound();
            }

            return _mapper.Map<List<MovieCharacterDTO>>(movieCharacters);
        }

        // GET: api/v1/MovieCharacters/bymovie/5
        /// <summary>
        /// Lists all MovieCharacter objects that have been part of a given Movie
        /// </summary>
        /// <param name="id">MovieID</param>
        /// <returns></returns>
        [HttpGet("bymovie/{id}")]
        public async Task<ActionResult<IEnumerable<MovieCharacterDTO>>> GetMovieCharactersByMovie(int id)
        {
            var movieCharacters = await _context.MovieCharacters.
                Include(mc => mc.Actor).
                Include(mc => mc.Character).
                Include(mc => mc.Movie).
                Where(mc => mc.MovieId == id).ToListAsync();

            if (movieCharacters.Count == 0)
            {
                return NotFound();
            }

            return _mapper.Map<List<MovieCharacterDTO>>(movieCharacters);
        }

        // PUT: api/v1/MovieCharacters
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut]
        public async Task<IActionResult> PutMovieCharacter(MovieCharacterIdDTO mcdto)
        {
            MovieCharacter mc = _mapper.Map<MovieCharacter>(mcdto);
            _context.Entry(mc).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MovieCharacterExists(mc.ActorId, mc.CharacterId, mc.MovieId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/v1/MovieCharacters
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<MovieCharacterDTO>> PostMovieCharacter(MovieCharacterIdDTO mcdto)
        {
            EntityEntry<MovieCharacter> mc = _context.MovieCharacters.Add(_mapper.Map<MovieCharacter>(mcdto));
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (MovieCharacterExists(mc.Entity.ActorId, mc.Entity.CharacterId, mc.Entity.MovieId))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction(nameof(GetMovieCharacterByID), new { mc.Entity.ActorId, mc.Entity.CharacterId, mc.Entity.MovieId }, mc.Entity);
        }

        // DELETE: api/v1/MovieCharacters
        [HttpDelete]
        public async Task<ActionResult<MovieCharacterDTO>> DeleteMovieCharacter(MovieCharacterIdDTO mcidto)
        {
            var movieCharacter = await _context.MovieCharacters.
                Where(mc => mc.ActorId == mcidto.ActorId && mc.CharacterId == mcidto.CharacterId && mc.MovieId == mcidto.MovieId).
                Include(mc => mc.Actor).Include(mc => mc.Character).Include(mc => mc.Movie).SingleOrDefaultAsync();
            if (movieCharacter == null)
            {
                return NotFound();
            }

            _context.MovieCharacters.Remove(movieCharacter);
            await _context.SaveChangesAsync();

            return _mapper.Map<MovieCharacterDTO>(movieCharacter);
        }

        private bool MovieCharacterExists(int actorId, int characterId, int movieId)
        {
            return _context.MovieCharacters.Any(e => e.ActorId == actorId && e.CharacterId == characterId && e.MovieId == movieId);
        }
    }
}
