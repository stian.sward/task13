﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using task13.Models;
using task13.DTOs.Actor;
using AutoMapper;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace task13.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class ActorsController : ControllerBase
    {
        private readonly MovieDbContext _context;
        private readonly IMapper _mapper;
        public ActorsController(MovieDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/v1/Actors
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ActorDTO>>> GetActors()
        {
            return _mapper.Map<IEnumerable<Actor>, List<ActorDTO>>(await _context.Actors.ToListAsync());
        }

        // GET: api/v1/Actors/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ActorDTO>> GetActor(int id)
        {
            var actor = _mapper.Map<Actor, ActorDTO>(await _context.Actors.FindAsync(id));

            if (actor == null)
            {
                return NotFound();
            }

            return actor;
        }

        // GET: api/v1/Actors/inmovie/5
        /// <summary>
        /// Returns a list of all the characters that have played in a movie, given the movie's Id
        /// </summary>
        /// <param name="id">MovieId</param>
        /// <returns>List of actors who have played that movie</returns>
        [HttpGet("inmovie/{id}")]
        public async Task<ActionResult<IEnumerable<ActorRoleDTO>>> GetActorsInMovie(int id)
        {
            var actors = _mapper.Map<IEnumerable<MovieCharacter>, List<ActorRoleDTO>>(await _context.MovieCharacters.
                Where(mc => mc.MovieId == id).
                Include(mc => mc.Actor).
                Include(mc => mc.Character).
                Include(mc => mc.Movie).
                ToListAsync());
            if (actors.Count == 0)
            {
                return NotFound();
            }
            return Ok(actors);
        }

        // GET: api/v1/Actors/playedcharacter/5
        /// <summary>
        /// Gets all the actors who have played a given character
        /// </summary>
        /// <param name="id">CharacterId</param>
        /// <returns>List of Actors</returns>
        [HttpGet("playedcharacter/{id}")]
        public async Task<ActionResult<IEnumerable<ActorDTO>>> GetActorsByCharacter(int id)
        {
            var actors = _mapper.Map<IEnumerable<Actor>, List<ActorDTO>>(await _context.MovieCharacters.
                Include(mc => mc.Character).
                Include(mc => mc.Actor).
                Where(mc => mc.CharacterId == id).
                Select(mc => mc.Actor).Distinct().ToListAsync());
            if (actors.Count == 0)
            {
                return NotFound();
            }
            return Ok(actors);
        }

        // PUT: api/v1/Actors/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutActor(int id, ActorDTO actor)
        {
            Actor a = _mapper.Map<Actor>(actor);
            a.Id = id;
            _context.Entry(a).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ActorExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/v1/Actors
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<ActorDTO>> PostActor(ActorDTO adto)
        {
            EntityEntry<Actor> actor = _context.Actors.Add(_mapper.Map<Actor>(adto));
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(GetActor), new { id = actor.Entity.Id }, actor.Entity);
        }

        // DELETE: api/v1/Actors/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<ActorDTO>> DeleteActor(int id)
        {
            var actor = await _context.Actors.FindAsync(id);
            if (actor == null)
            {
                return NotFound();
            }

            _context.Actors.Remove(actor);
            await _context.SaveChangesAsync();

            return _mapper.Map<Actor, ActorDTO>(actor);
        }

        private bool ActorExists(int id)
        {
            return _context.Actors.Any(e => e.Id == id);
        }
    }
}
