﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using task13.DTOs.Movie;
using task13.DTOs.Character;
using task13.Models;
using task13.DTOs.Franchise;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace task13.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class FranchisesController : ControllerBase
    {
        private readonly MovieDbContext _context;
        private readonly IMapper _mapper;

        public FranchisesController(MovieDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/v1/Franchises
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseDTO>>> GetFranchises()
        {
            var franchises = _mapper.Map<IEnumerable<Franchise>, List<FranchiseDTO>>(await _context.Franchises.ToListAsync());
            if (franchises.Count == 0)
            {
                return NotFound();
            }
            return franchises;
        }

        // GET: api/v1/Franchises/5
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseDTO>> GetFranchise(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);

            if (franchise == null)
            {
                return NotFound();
            }

            return _mapper.Map<FranchiseDTO>(franchise);
        }

        // GET: api/v1/Franchises/movies/5
        /// <summary>
        /// Lists all the movies in a given Franchise
        /// </summary>
        /// <param name="id">FranchiseID</param>
        /// <returns></returns>
        [HttpGet("movies/{id}")]
        public async Task<ActionResult<IEnumerable<MovieDTO>>> GetMoviesInFranchise(int id)
        {
            return _mapper.Map<List<Movie>, List<MovieDTO>>(await _context.Movies.
                Where(m => m.FranchiseId == id).ToListAsync());
        }

        // GET: api/v1/Franchises/characters/5
        /// <summary>
        /// Lists all the Characters that have been part of a given Franchise
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("characters/{id}")]
        public async Task<ActionResult<IEnumerable<CharacterDTO>>> GetCharactersInFranchise(int id)
        {
            var characters = _mapper.Map<List<Character>, List<CharacterDTO>>(await _context.Franchises.
                Where(f => f.Id == id).
                SelectMany(f => f.Movies).
                SelectMany(m => m.Cast).
                Select(mc => mc.Character).Distinct().ToListAsync());

            if (characters.Count == 0)
            {
                return NotFound();
            }
            return Ok(characters);
        }

        // PUT: api/v1/Franchises/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchise(int id, FranchiseDTO franchise)
        {
            Franchise f = _mapper.Map<Franchise>(franchise);
            f.Id = id;
            _context.Entry(f).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FranchiseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/v1/Franchises
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<FranchiseDTO>> PostFranchise(FranchiseDTO fdto)
        {
            EntityEntry<Franchise> franchise = _context.Franchises.Add(_mapper.Map<Franchise>(fdto));
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(GetFranchise), new { id = franchise.Entity.Id }, franchise.Entity);
        }

        // DELETE: api/v1/Franchises/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<FranchiseDTO>> DeleteFranchise(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);
            if (franchise == null)
            {
                return NotFound();
            }

            _context.Franchises.Remove(franchise);
            await _context.SaveChangesAsync();

            return _mapper.Map<FranchiseDTO>(franchise);
        }

        private bool FranchiseExists(int id)
        {
            return _context.Franchises.Any(e => e.Id == id);
        }
    }
}
