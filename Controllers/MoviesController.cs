﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using task13.DTOs.Movie;
using task13.Models;

namespace task13.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class MoviesController : ControllerBase
    {
        private readonly MovieDbContext _context;
        private readonly IMapper _mapper;

        public MoviesController(MovieDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/v1/Movies
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieDTO>>> GetMovies()
        {
            var movies = await _context.Movies.Include(m => m.Director).Include(m => m.Franchise).ToListAsync();
            if (movies.Count == 0)
            {
                return NotFound();
            }
            return _mapper.Map<IEnumerable<Movie>, List<MovieDTO>>(movies);
        }

        // GET: api/v1/Movies/5
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieDTO>> GetMovie(int id)
        {
            var movie = await _context.Movies.Where(m => m.Id == id).Include(m => m.Director).Include(m => m.Franchise).SingleOrDefaultAsync();

            if (movie == null)
            {
                return NotFound();
            }

            return _mapper.Map<MovieDTO>(movie);
        }

        // GET: api/v1/Movies/withactor/5
        /// <summary>
        /// Creates a list of all the movies a given actor has played in
        /// </summary>
        /// <param name="actorId">ActorID</param>
        /// <returns>List of movies with the given actor</returns>
        [HttpGet("withactor/{actorId}")]
        public async Task<ActionResult<IEnumerable<MovieDTO>>> GetMoviesWithActor(int actorId)
        {
            var movies = _mapper.Map<IEnumerable<Movie>, List<MovieDTO>>(await _context.MovieCharacters.
                Include(mc => mc.Movie).
                Where(mc => mc.ActorId == actorId).
                Select(mc => mc.Movie).Distinct().Include(m => m.Director).Include(m => m.Franchise).ToListAsync());
            if (movies.Count == 0)
            {
                return NotFound();
            }
            return Ok(movies);
        }

        // PUT: api/v1/Movies/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovie(int id, MovieSimplifiedDTO mdto)
        {
            Movie movie = _mapper.Map<Movie>(mdto);
            movie.Id = id;

            _context.Entry(movie).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MovieExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/v1/Movies
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<MovieDTO>> PostMovie(MovieSimplifiedDTO mdto)
        {
            EntityEntry<Movie> movie = _context.Movies.Add(_mapper.Map<Movie>(mdto));
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(GetMovie), new { id = movie.Entity.Id }, movie.Entity);
        }

        // DELETE: api/v1/Movies/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<MovieDTO>> DeleteMovie(int id)
        {
            var movie = await _context.Movies.Where(m => m.Id == id).Include(m => m.Director).Include(m => m.Franchise).SingleOrDefaultAsync();
            if (movie == null)
            {
                return NotFound();
            }

            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();

            return _mapper.Map<MovieDTO>(movie);
        }

        private bool MovieExists(int id)
        {
            return _context.Movies.Any(e => e.Id == id);
        }
    }
}
