﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using task13.DTOs.Director;
using task13.Models;

namespace task13.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class DirectorsController : ControllerBase
    {
        private readonly MovieDbContext _context;
        private readonly IMapper _mapper;

        public DirectorsController(MovieDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/v1/Directors
        [HttpGet]
        public async Task<ActionResult<IEnumerable<DirectorDTO>>> GetDirectors()
        {
            return _mapper.Map<IEnumerable<Director>, List<DirectorDTO>>(await _context.Directors.ToListAsync());
        }

        // GET: api/v1/Directors/5
        [HttpGet("{id}")]
        public async Task<ActionResult<DirectorDTO>> GetDirector(int id)
        {
            var director = await _context.Directors.FindAsync(id);

            if (director == null)
            {
                return NotFound();
            }

            return _mapper.Map<DirectorDTO>(director);
        }

        // PUT: api/v1/Directors/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDirector(int id, DirectorDTO director)
        {
            Director d = _mapper.Map<Director>(director);
            d.Id = id;
            _context.Entry(d).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DirectorExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/v1/Directors
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<DirectorDTO>> PostDirector(DirectorDTO ddto)
        {
            EntityEntry<Director> director = _context.Directors.Add(_mapper.Map<Director>(ddto));
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(GetDirector), new { Id = director.Entity.Id }, director.Entity);
        }

        // DELETE: api/v1/Directors/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Director>> DeleteDirector(int id)
        {
            var director = await _context.Directors.FindAsync(id);
            if (director == null)
            {
                return NotFound();
            }

            _context.Directors.Remove(director);
            await _context.SaveChangesAsync();

            return director;
        }

        private bool DirectorExists(int id)
        {
            return _context.Directors.Any(e => e.Id == id);
        }
    }
}
