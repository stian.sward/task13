﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using task13.Models;
using task13.DTOs;
using task13.DTOs.Character;
using AutoMapper;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace task13.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class CharactersController : ControllerBase
    {
        private readonly MovieDbContext _context;
        private readonly IMapper _mapper;

        public CharactersController(MovieDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/v1/Characters
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Character>>> GetCharacters()
        {
            return await _context.Characters.ToListAsync();
        }

        // GET: api/v1/Characters/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Character>> GetCharacter(int id)
        {
            var character = await _context.Characters.FindAsync(id);

            if (character == null)
            {
                return NotFound();
            }

            return Ok(character);
        }

        // GET: api/v1/Characters/frommovie/5
        /// <summary>
        /// Gets a list of the Characters who have been in a given movie
        /// </summary>
        /// <param name="id">MovieID</param>
        /// <returns></returns>
        [HttpGet("frommovie/{id}")]
        public async Task<ActionResult<List<CharacterWithActorDTO>>> GetCharactersFromMovie(int id)
        {
            var characters = _mapper.Map<List<MovieCharacter>, List<CharacterWithActorDTO>>(await _context.MovieCharacters.
                Include(mc => mc.Movie).
                Include(mc => mc.Character).
                Include(mc => mc.Actor).
                Where(mc => mc.Movie.Id == id).
                ToListAsync());

            if (characters.Count == 0)
            {
                return NotFound();
            }
            return Ok(characters);
        }

        // GET: api/v1/Character/byactor/5
        /// <summary>
        /// Gets a list of the Characters that have been played by a given actor
        /// </summary>
        /// <param name="id">ActorID</param>
        /// <returns></returns>
        [HttpGet("byactor/{id}")]
        public async Task<ActionResult<List<CharacterDTO>>> GetCharactersByActor(int id)
        {
            var characters = _mapper.Map<List<Character>, List<CharacterDTO>>(await _context.MovieCharacters.
                Include(mc => mc.Character).
                Where(mc => mc.ActorId == id).
                Select(mc => mc.Character).Distinct().ToListAsync());
            if (characters.Count == 0)
            {
                return NotFound();
            }
            return Ok(characters);
        }

        // PUT: api/v1/Characters/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCharacter(int id, CharacterDTO character)
        {
            Character c = _mapper.Map<Character>(character);
            c.Id = id;
            _context.Entry(c).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CharacterExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/v1/Characters
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<CharacterDTO>> PostCharacter(CharacterDTO cdto)
        {
            EntityEntry<Character> character = _context.Characters.Add(_mapper.Map<Character>(cdto));
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(GetCharacter), new { Id = character.Entity.Id }, character.Entity);
        }

        // DELETE: api/v1/Characters/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<CharacterDTO>> DeleteCharacter(int id)
        {
            var character = await _context.Characters.FindAsync(id);
            if (character == null)
            {
                return NotFound();
            }

            _context.Characters.Remove(character);
            await _context.SaveChangesAsync();

            return _mapper.Map<CharacterDTO>(character);
        }

        private bool CharacterExists(int id)
        {
            return _context.Characters.Any(e => e.Id == id);
        }
    }
}
