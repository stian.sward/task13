# task13

## Movie Database

### Notes:

* Remember to change the Database String in Startup.cs. I suggest changing the call to "default" and changing the "default" entry in appsettings.json from "(localdb)" to whatever you need.

* The Postman collection is set up to be run once, and not make any significant changes to the database compared to the InitialDB migration. Therefore, running the collection more than once without resetting the database to InitialDB will result in some errors. These will mainly be 404 responses because Postman is trying to delete an ID that was used and then deleted the first time it was run, and the database has now incremented past them.