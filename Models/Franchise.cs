﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace task13.Models
{
    public class Franchise
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        // Relationships
        public ICollection<Movie> Movies { get; set; }
    }
}
