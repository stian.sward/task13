﻿using System;
using System.Collections.Generic;
using System.Text;

namespace task13.Models
{
    public class Director : Person
    {
        public ICollection<Movie> Movies { get; set; }
    }
}
