﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace task13.Models
{
    public class MovieDbContext : DbContext
    {
        public DbSet<Actor> Actors { get; set; }
        public DbSet<Character> Characters { get; set; }
        public DbSet<Director> Directors { get; set; }
        public DbSet<Franchise> Franchises { get; set; }
        public DbSet<Movie> Movies { get; set; }
        public DbSet<MovieCharacter> MovieCharacters { get; set; }

        public MovieDbContext(DbContextOptions<MovieDbContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            // Composite Keys
            builder.Entity<MovieCharacter>().HasKey(mc => new { mc.ActorId, mc.CharacterId, mc.MovieId });

            // Seed data
            // Actors
            IList<Actor> actors = new List<Actor>
            {
                new Actor
                {
                    Id = 1,
                    FirstName = "Daniel",
                    LastName = "Radcliffe",
                    Gender = "Male",
                    DOB = new DateTime(1989, 6, 23),
                    PlaceOfBirth = "Fulham, London, England, UK",
                    PicURL = "https://m.media-amazon.com/images/M/MV5BMTg4NTExODc3Nl5BMl5BanBnXkFtZTgwODUyMDEzMDE@._V1_.jpg"
                },
                new Actor
                {
                    Id = 2,
                    FirstName = "Rupert",
                    LastName = "Grint",
                    Gender = "Male",
                    DOB = new DateTime(1988, 8, 24),
                    PlaceOfBirth = "Stevenage, Hertfordshire, England, UK",
                    PicURL = "https://m.media-amazon.com/images/M/MV5BMjI3MDA3NjA1N15BMl5BanBnXkFtZTcwMDcyMDYzNw@@._V1_.jpg"
                },
                new Actor
                {
                    Id = 3,
                    FirstName = "Emma",
                    LastName = "Watson",
                    Gender = "Female",
                    DOB = new DateTime(1990, 4, 15),
                    PlaceOfBirth = "Paris, France",
                    PicURL = "https://m.media-amazon.com/images/M/MV5BMTQ3ODE2NTMxMV5BMl5BanBnXkFtZTgwOTIzOTQzMjE@._V1_SY1000_CR0,0,810,1000_AL_.jpg"
                },
                new Actor
                {
                    Id = 4,
                    FirstName = "Richard",
                    LastName = "Harris",
                    Gender = "Male",
                    DOB = new DateTime(1930, 10, 1),
                    PlaceOfBirth = "Limerick, Ireland",
                    PicURL = "https://m.media-amazon.com/images/M/MV5BMTgzNTA5ODg1NV5BMl5BanBnXkFtZTcwMDU3MTU5Mw@@._V1_.jpg"
                },
                new Actor
                {
                    Id = 5,
                    FirstName = "Michael",
                    LastName = "Gambon",
                    Gender = "Male",
                    DOB = new DateTime(1940, 10, 19),
                    PlaceOfBirth = "Cabra, Dublin, Ireland",
                    PicURL = "https://m.media-amazon.com/images/M/MV5BMTY3OTc4MTgyN15BMl5BanBnXkFtZTcwNTAxNjA3Mg@@._V1_.jpg"
                },
                new Actor
                {
                    Id = 6,
                    FirstName = "Richard",
                    LastName = "Bremmer",
                    Gender = "Male",
                    DOB = new DateTime(1953, 1, 27),
                    PlaceOfBirth = "Warwickshire, England, UK",
                    PicURL = "https://m.media-amazon.com/images/M/MV5BMjA2NTA4NzIyM15BMl5BanBnXkFtZTcwNTY5MzAwOA@@._V1_.jpg"
                },
                new Actor
                {
                    Id = 7,
                    FirstName = "Ralph",
                    LastName = "Fiennes",
                    Gender = "Male",
                    DOB = new DateTime(1962, 12, 22),
                    PlaceOfBirth = "Ipswich, Suffolk, England, UK",
                    PicURL = "https://m.media-amazon.com/images/M/MV5BMzc5MjE1NDgyN15BMl5BanBnXkFtZTcwNzg2ODgwNA@@._V1_.jpg"
                },
                new Actor
                {
                    Id = 8,
                    FirstName = "Elijah",
                    LastName = "Wood",
                    Gender = "Male",
                    DOB = new DateTime(1981, 1, 28),
                    PlaceOfBirth = "Cedar Rapis, Iowa, USA",
                    PicURL = "https://m.media-amazon.com/images/M/MV5BMTM0NDIxMzQ5OF5BMl5BanBnXkFtZTcwNzAyNTA4Nw@@._V1_SY1000_CR0,0,666,1000_AL_.jpg"
                },
                new Actor
                {
                    Id = 9,
                    FirstName = "Ian",
                    LastName = "McKellen",
                    Gender = "Male",
                    DOB = new DateTime(1939, 5, 25),
                    PlaceOfBirth = "Burnley, Lancashire, England, UK",
                    PicURL = "https://m.media-amazon.com/images/M/MV5BMTQ2MjgyNjk3MV5BMl5BanBnXkFtZTcwNTA3NTY5Mg@@._V1_.jpg"
                },
                new Actor
                {
                    Id = 10,
                    FirstName = "Viggo",
                    LastName = "Mortensen",
                    Gender = "Male",
                    DOB = new DateTime(1958, 10, 20),
                    PlaceOfBirth = "Manhattan, New York City, New York, USA",
                    PicURL = "https://m.media-amazon.com/images/M/MV5BNDQzOTg4NzA2Nl5BMl5BanBnXkFtZTcwMzkwNjkxMg@@._V1_.jpg"
                },
                new Actor
                {
                    Id = 11,
                    FirstName = "Liv",
                    LastName = "Tyler",
                    Gender = "Female",
                    DOB = new DateTime(1977, 7, 1),
                    PlaceOfBirth = "New York City, New York, USA",
                    PicURL = "https://m.media-amazon.com/images/M/MV5BMTY4NjQxMjc5MF5BMl5BanBnXkFtZTcwMzg5Mzg4Ng@@._V1_SY1000_CR0,0,666,1000_AL_.jpg"
                },
                new Actor
                {
                    Id = 12,
                    FirstName = "Sean",
                    LastName = "Bean",
                    Gender = "Male",
                    DOB = new DateTime(1959, 4, 17),
                    PlaceOfBirth = "Sheffield, South Yorkshire, England, UK",
                    PicURL = "https://m.media-amazon.com/images/M/MV5BMTkzMzc4MDk5OF5BMl5BanBnXkFtZTcwODg3MjUxNw@@._V1_SY1000_CR0,0,726,1000_AL_.jpg"
                },
                new Actor
                {
                    Id = 13,
                    FirstName = "Orlando",
                    LastName = "Bloom",
                    MiddleNames = "Jonathan Blanchard",
                    Gender = "Male",
                    DOB = new DateTime(1977, 1, 13),
                    PlaceOfBirth = "Canterbury, Kent, England, UK",
                    PicURL = "https://m.media-amazon.com/images/M/MV5BMjE1MDkxMjQ3NV5BMl5BanBnXkFtZTcwMzQ3Mjc4MQ@@._V1_.jpg"
                },
                new Actor
                {
                    Id = 14,
                    FirstName = "Billy",
                    LastName = "Boyd",
                    Gender = "Male",
                    DOB = new DateTime(1968, 8, 28),
                    PlaceOfBirth = "Glasgow, Scotland, UK",
                    PicURL = "https://m.media-amazon.com/images/M/MV5BMTQzOTEyMTQ3OF5BMl5BanBnXkFtZTYwNzkzMDk1._V1_.jpg"
                },
                new Actor
                {
                    Id = 15,
                    FirstName = "Dominic",
                    LastName = "Monaghan",
                    Gender = "Male",
                    DOB = new DateTime(1976, 12, 8),
                    PlaceOfBirth = "Berlin, Germany",
                    PicURL = "https://m.media-amazon.com/images/M/MV5BMTg0MTc2ODIwNl5BMl5BanBnXkFtZTcwMjQ4MjEwMw@@._V1_.jpg"
                },
                new Actor
                {
                    Id = 16,
                    FirstName = "John",
                    LastName = "Rhys-Davies",
                    Gender = "Male",
                    DOB = new DateTime(1944, 5, 5),
                    PlaceOfBirth = "Ammanford, Wales, UK",
                    PicURL = "https://m.media-amazon.com/images/M/MV5BMjMwNDY3NjQxMF5BMl5BanBnXkFtZTcwMDc3NTYyOQ@@._V1_SY1000_CR0,0,837,1000_AL_.jpg"
                }
            };

            // Characters
            IList<Character> characters = new List<Character>
            {
                new Character
                {
                    Id = 1,
                    FullName = "Harry Potter",
                    Alias = "Harry",
                    Gender = "Male",
                    PicURL = "https://www.irishtimes.com/polopoly_fs/1.3170107.1501253408!/image/image.jpg_gen/derivatives/ratio_1x1_w1200/image.jpg"
                },
                new Character
                {
                    Id = 2,
                    FullName = "Ronald Weasley",
                    Alias = "Ron",
                    Gender = "Male",
                    PicURL = "https://m.media-amazon.com/images/M/MV5BMTkyMDM1MTQzOF5BMl5BanBnXkFtZTYwMzg0NTg3._V1_.jpg"
                },
                new Character
                {
                    Id = 3,
                    FullName = "Hermione Granger",
                    Alias = "Hermione",
                    Gender = "Female",
                    PicURL = "https://m.media-amazon.com/images/M/MV5BN2ZmNmRlNDQtM2M4Zi00N2M3LWEwNmEtOTE0ZGY3OTZlYjFiXkEyXkFqcGdeQXVyNjQ4ODE4MzQ@._V1_SY1000_CR0,0,663,1000_AL_.jpg"
                },
                new Character
                {
                    Id = 4,
                    FullName = "Albus Percival Wulfric Brian Dumbledore",
                    Alias = "Professor Dumbledore",
                    Gender = "Male",
                    PicURL = "https://m.media-amazon.com/images/M/MV5BZTgzOTNhNzItNWNlYi00YzdhLWE4ODktMDhlYWYxYzZmZDU3XkEyXkFqcGdeQXVyMjI3NzE4MTM@._V1_.jpg"
                },
                new Character
                {
                    Id = 5,
                    FullName = "Tom Marvolo Riddle",
                    Alias = "Lord Voldemort",
                    Gender = "Male",
                    PicURL = "https://m.media-amazon.com/images/M/MV5BMTY4ODgzNjEwOV5BMl5BanBnXkFtZTcwMTg3MDI5Mw@@._V1_SY1000_CR0,0,1678,1000_AL_.jpg"
                },
                new Character
                {
                    Id = 6,
                    FullName = "Frodo Baggins",
                    Alias = "Sneaky hobbit",
                    Gender = "Male",
                    PicURL = "https://m.media-amazon.com/images/M/MV5BMTYxNjY1ODQ3MF5BMl5BanBnXkFtZTcwMjgwMjk2Mw@@._V1_SY1000_CR0,0,1489,1000_AL_.jpg"
                },
                new Character
                {
                    Id = 7,
                    FullName = "Gandalf",
                    Alias = "Gandalf the Grey, Gandalf the White",
                    Gender = "Male",
                    PicURL = "https://m.media-amazon.com/images/M/MV5BMTY1OTE1NDY4NV5BMl5BanBnXkFtZTcwMzkwMjk2Mw@@._V1_SY1000_CR0,0,1501,1000_AL_.jpg"
                },
                new Character
                {
                    Id = 8,
                    FullName = "Aragorn",
                    Alias = "Strider",
                    Gender = "Male",
                    PicURL = "https://m.media-amazon.com/images/M/MV5BNzc4NzYyNDk0MV5BMl5BanBnXkFtZTcwMTExMjk2Mw@@._V1_SY1000_CR0,0,736,1000_AL_.jpg"
                },
                new Character
                {
                    Id = 9,
                    FullName = "Arwen",
                    Alias = "Undómiel",
                    Gender = "Female",
                    PicURL = "https://m.media-amazon.com/images/M/MV5BNzdlMWUxYTktYTJlYi00M2RkLWJlNzAtMzM5YzUwNmE1MzlmXkEyXkFqcGdeQXVyMzQ3Nzk5MTU@._V1_.jpg"
                },
                new Character
                {
                    Id = 10,
                    FullName = "Boromir",
                    Gender = "Male",
                    PicURL = "https://m.media-amazon.com/images/M/MV5BNzRjODZjMzktMTY4NS00ZWIzLWIxYTgtMTYzZWMzYjZhODkwXkEyXkFqcGdeQXVyNzU3Nzk4MDQ@._V1_.jpg"
                },
                new Character
                {
                    Id = 11,
                    FullName = "Legolas",
                    Alias = "Greenleaf",
                    Gender = "Male",
                    PicURL = "https://m.media-amazon.com/images/M/MV5BMDM5YzJlNmItZWJiZS00Y2ZkLTkyZGUtZjllMDMxMDk3OWMzXkEyXkFqcGdeQXVyMzQ3Nzk5MTU@._V1_.jpg"
                },
                new Character
                {
                    Id = 12,
                    FullName = "Peregrin Took",
                    Alias = "Pippin",
                    Gender = "Male",
                    PicURL = "https://m.media-amazon.com/images/M/MV5BNDQ4MThiZjEtNDdhMC00NTU4LWIxYzItMDgzMDVlZjlhYzk0XkEyXkFqcGdeQXVyMzQ3Nzk5MTU@._V1_.jpg"
                },
                new Character
                {
                    Id = 13,
                    FullName = "Meriadoc Brandybuck",
                    Alias = "Merry",
                    Gender = "Male",
                    PicURL = "https://m.media-amazon.com/images/M/MV5BMTI4ODY2NjMyNF5BMl5BanBnXkFtZTcwMjkwMjk2Mw@@._V1_SY1000_CR0,0,1479,1000_AL_.jpg"
                },
                new Character
                {
                    Id = 14,
                    FullName = "Gimli",
                    Alias = "Elf-friend",
                    Gender = "Male",
                    PicURL = "https://m.media-amazon.com/images/M/MV5BMTg2NDYxMzQ0Ml5BMl5BanBnXkFtZTcwNzExMjk2Mw@@._V1_SY1000_CR0,0,1497,1000_AL_.jpg"
                }
            };

            // Director
            IList<Director> directors = new List<Director>
            {
                new Director
                {
                    Id = 1,
                    FirstName = "Chris",
                    LastName = "Columbus",
                    Gender = "Male",
                    DOB = new DateTime(1958, 9, 10),
                    PlaceOfBirth = "Spangler, Pennsylvania, USA",
                    PicURL = "https://m.media-amazon.com/images/M/MV5BMTY2MTYzNzUyNl5BMl5BanBnXkFtZTYwMDI0NzA0._V1_.jpg"
                },
                new Director
                {
                    Id = 2,
                    FirstName = "Alfonso",
                    LastName = "Cuarón",
                    Gender = "Male",
                    DOB = new DateTime(1961, 11, 28),
                    PlaceOfBirth = "Mexico City, Distrito Federal, Mexico",
                    PicURL = "https://m.media-amazon.com/images/M/MV5BMjA0ODY4OTk4Nl5BMl5BanBnXkFtZTcwNTkxMzYyMg@@._V1_.jpg"
                },
                new Director
                {
                    Id = 3,
                    FirstName = "Mike",
                    LastName = "Newell",
                    Gender = "Male",
                    DOB = new DateTime(1942, 3, 28),
                    PlaceOfBirth = "St. Albans, Hertfordshire, England, UK",
                    PicURL = "https://m.media-amazon.com/images/M/MV5BMTgzNDI1ODc4N15BMl5BanBnXkFtZTYwNjg3NTc1._V1_.jpg"
                },
                new Director
                {
                    Id = 4,
                    FirstName = "David",
                    LastName = "Yates",
                    Gender = "Male",
                    DOB = new DateTime(1963, 10, 8),
                    PlaceOfBirth = "St. Helens, Merseyside, England, UK",
                    PicURL = "https://m.media-amazon.com/images/M/MV5BMTY2NTU4NjY4M15BMl5BanBnXkFtZTYwNjIxOTI1._V1_.jpg"
                },
                new Director
                {
                    Id = 5,
                    FirstName = "Peter",
                    LastName = "Jackson",
                    Gender = "Male",
                    DOB = new DateTime(1961, 10, 31),
                    PlaceOfBirth = "Pukerua Bay, North Island, New Zealand",
                    PicURL = "https://m.media-amazon.com/images/M/MV5BMTY1MzQ3NjA2OV5BMl5BanBnXkFtZTcwNTExOTA5OA@@._V1_SY1000_CR0,0,725,1000_AL_.jpg"
                }
            };

            // Franchises
            IList<Franchise> franchises = new List<Franchise>
            {
                new Franchise
                {
                    Id = 1,
                    Name = "Harry Potter",
                    Description = "The Wizarding World of Harry Potter"
                },
                new Franchise
                {
                    Id = 2,
                    Name = "The Lord of the Rings",
                    Description = "Peter Jackson's trilogy based on J. R. R. Tolkien's books"
                }
            };

            // Movies
            IList<Movie> movies = new List<Movie>
            {
                new Movie
                {
                    Id = 1,
                    Title = "Harry Potter and the Philosopher's Stone",
                    ReleaseYear = new DateTime(2001, 11, 16),
                    Genre = "Adventure, Family, Fantasy",
                    DirectorId = 1,
                    Synopsis = "An orphaned boy enrolls in a school of wizardry, where he learns the truth about himself, his family and the terrible evil that haunts the magical world.",
                    PicURL = "https://i.ibb.co/mHMF0jr/1-HP-Philosopher.png",
                    TrailerURL = "https://www.youtube.com/watch?v=VyHV0BRtdxo",
                    FranchiseId = 1
                },
                new Movie
                {
                    Id = 2,
                    Title = "Harry Potter and the Chamber of Secrets",
                    ReleaseYear = new DateTime(2002, 11, 15),
                    Genre = "Adventure, Family, Fantasy",
                    DirectorId = 1,
                    Synopsis = "An ancient prophecy seems to be coming true when a mysterious presence begins stalking the corridors of a school of magic and leaving its victims paralyzed.",
                    PicURL = "https://i.ibb.co/QmvWWqb/2-HP-Chamber.png",
                    TrailerURL = "https://www.youtube.com/watch?v=1bq0qff4iF8",
                    FranchiseId = 1
                },
                new Movie
                {
                    Id = 3,
                    Title = "Harry Potter and the Prisoner of Azkaban",
                    ReleaseYear = new DateTime(2004, 5, 31),
                    Genre = "Adventure, Family, Fantasy",
                    DirectorId = 2,
                    Synopsis = "Harry Potter, Ron and Hermione return to Hogwarts School of Witchcraft and Wizardry for their third year of study, where they delve into the mystery surrounding an escaped prisoner who poses a dangerous threat to the young wizard.",
                    PicURL = "https://i.ibb.co/2dPm1yk/3-HP-Prisoner.png",
                    TrailerURL = "https://www.youtube.com/watch?v=1ZdlAg3j8nI",
                    FranchiseId = 1
                },
                new Movie
                {
                    Id = 4,
                    Title = "Harry Potter and the Goblet of Fire",
                    ReleaseYear = new DateTime(2005, 11, 18),
                    Genre = "Adventure, Family, Fantasy",
                    DirectorId = 3,
                    Synopsis = "Harry Potter finds himself competing in a hazardous tournament between rival schools of magic, but he is distracted by recurring nightmares.",
                    PicURL = "https://i.ibb.co/my6NLsn/4-HP-Goblet.png",
                    TrailerURL = "https://www.youtube.com/watch?v=3EGojp4Hh6I",
                    FranchiseId = 1
                },
                new Movie
                {
                    Id = 5,
                    Title = "Harry Potter and the Order of the Phoenix",
                    ReleaseYear = new DateTime(2007, 7, 12),
                    Genre = "Action, Adventure, Family",
                    DirectorId = 4,
                    Synopsis = "With their warning about Lord Voldemort's return scoffed at, Harry and Dumbledore are targeted by the Wizard authorities as an authoritarian bureaucrat slowly seizes power at Hogwarts.",
                    PicURL = "https://i.ibb.co/GPtWH4w/5-HP-Order.png",
                    TrailerURL = "https://www.youtube.com/watch?v=y6ZW7KXaXYk",
                    FranchiseId = 1
                },
                new Movie
                {
                    Id = 6,
                    Title = "Harry Potter and the Half-Blood Prince",
                    ReleaseYear = new DateTime(2009, 7, 15),
                    Genre = "Action, Adventure, Family",
                    DirectorId = 4,
                    Synopsis = "As Harry Potter begins his sixth year at Hogwarts, he discovers an old book marked as 'the property of the Half - Blood Prince' and begins to learn more about Lord Voldemort's dark past.",
                    PicURL = "https://i.ibb.co/PwKZB23/6-HP-Half-Blood.png",
                    TrailerURL = "https://www.youtube.com/watch?v=sg81Lts5kYY",
                    FranchiseId = 1
                },
                new Movie
                {
                    Id = 7,
                    Title = "Harry Potter and the Deathly Hallows: Part 1",
                    ReleaseYear = new DateTime(2010, 11, 19),
                    Genre = "Adventure, Family, Fantasy",
                    DirectorId = 4,
                    Synopsis = "As Harry, Ron, and Hermione race against time and evil to destroy the Horcruxes, they uncover the existence of the three most powerful objects in the wizarding world: the Deathly Hallows.",
                    PicURL = "https://i.ibb.co/dk2SxZb/7-HP-Deathly-P1.png",
                    TrailerURL = "https://www.youtube.com/watch?v=MxqsmsA8y5k",
                    FranchiseId = 1
                },
                new Movie
                {
                    Id = 8,
                    Title = "Harry Potter and the Deathly Hallows: Part 2",
                    ReleaseYear = new DateTime(2011, 7, 15),
                    Genre = "Adventure, Drama, Fantasy",
                    DirectorId = 4,
                    Synopsis = "Harry, Ron, and Hermione search for Voldemort's remaining Horcruxes in their effort to destroy the Dark Lord as the final battle rages on at Hogwarts.",
                    PicURL = "https://i.ibb.co/VjDwsf8/8-HP-Deathly-P2.png",
                    TrailerURL = "https://www.youtube.com/watch?v=mObK5XD8udk",
                    FranchiseId = 1
                },
                new Movie
                {
                    Id = 9,
                    Title = "The Lord of the Rings: The Fellowship of the Ring",
                    ReleaseYear = new DateTime(2001, 12, 19),
                    Genre = "Action, Adventure, Drama",
                    DirectorId = 5,
                    Synopsis = "A meek Hobbit from the Shire and eight companions set out on a journey to destroy the powerful One Ring and save Middle-earth from the Dark Lord Sauron.",
                    PicURL = "https://m.media-amazon.com/images/M/MV5BN2EyZjM3NzUtNWUzMi00MTgxLWI0NTctMzY4M2VlOTdjZWRiXkEyXkFqcGdeQXVyNDUzOTQ5MjY@._V1_SY999_CR0,0,673,999_AL_.jpg",
                    TrailerURL = "https://www.youtube.com/watch?v=V75dMMIW2B4",
                    FranchiseId = 2
                },
                new Movie
                {
                    Id = 10,
                    Title = "The Lord of the Rings: The Two Towers",
                    ReleaseYear = new DateTime(2002, 12, 18),
                    Genre = "Action, Adventure, Drama",
                    DirectorId = 5,
                    Synopsis = "While Frodo and Sam edge closer to Mordor with the help of the shifty Gollum, the divided fellowship makes a stand against Sauron's new ally, Saruman, and his hordes of Isengard.",
                    PicURL = "https://m.media-amazon.com/images/M/MV5BZGMxZTdjZmYtMmE2Ni00ZTdkLWI5NTgtNjlmMjBiNzU2MmI5XkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_SY1000_CR0,0,642,1000_AL_.jpg",
                    TrailerURL = "https://www.youtube.com/watch?v=LbfMDwc4azU",
                    FranchiseId = 2
                },
                new Movie
                {
                    Id = 11,
                    Title = "The Lord of the Rings: The Return of the King",
                    ReleaseYear = new DateTime(2003, 12, 17),
                    Genre = "Action, Adventure, Drama",
                    DirectorId = 5,
                    Synopsis = "Gandalf and Aragorn lead the World of Men against Sauron's army to draw his gaze from Frodo and Sam as they approach Mount Doom with the One Ring.",
                    PicURL = "https://m.media-amazon.com/images/M/MV5BNzA5ZDNlZWMtM2NhNS00NDJjLTk4NDItYTRmY2EwMWZlMTY3XkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_SY1000_CR0,0,675,1000_AL_.jpg",
                    TrailerURL = "https://www.youtube.com/watch?v=y2rYRu8UW8M",
                    FranchiseId = 2
                }
            };

            // MovieCharacters
            IList<MovieCharacter> movieCharacters = new List<MovieCharacter>
            {
                new MovieCharacter { ActorId = 1, CharacterId = 1, MovieId = 1, PicURL = "https://m.media-amazon.com/images/M/MV5BNTI0MTQzODYzM15BMl5BanBnXkFtZTYwNTc0NTg3._V1_.jpg" },
                new MovieCharacter { ActorId = 1, CharacterId = 1, MovieId = 2 },
                new MovieCharacter { ActorId = 1, CharacterId = 1, MovieId = 3 },
                new MovieCharacter { ActorId = 1, CharacterId = 1, MovieId = 4 },
                new MovieCharacter { ActorId = 1, CharacterId = 1, MovieId = 5 },
                new MovieCharacter { ActorId = 1, CharacterId = 1, MovieId = 6 },
                new MovieCharacter { ActorId = 1, CharacterId = 1, MovieId = 7 },
                new MovieCharacter { ActorId = 1, CharacterId = 1, MovieId = 8 },
                new MovieCharacter { ActorId = 2, CharacterId = 2, MovieId = 1 },
                new MovieCharacter { ActorId = 2, CharacterId = 2, MovieId = 2 },
                new MovieCharacter { ActorId = 2, CharacterId = 2, MovieId = 3 },
                new MovieCharacter { ActorId = 2, CharacterId = 2, MovieId = 4 },
                new MovieCharacter { ActorId = 2, CharacterId = 2, MovieId = 5 },
                new MovieCharacter { ActorId = 2, CharacterId = 2, MovieId = 6 },
                new MovieCharacter { ActorId = 2, CharacterId = 2, MovieId = 7 },
                new MovieCharacter { ActorId = 2, CharacterId = 2, MovieId = 8 },
                new MovieCharacter { ActorId = 3, CharacterId = 3, MovieId = 1 },
                new MovieCharacter { ActorId = 3, CharacterId = 3, MovieId = 2 },
                new MovieCharacter { ActorId = 3, CharacterId = 3, MovieId = 3 },
                new MovieCharacter { ActorId = 3, CharacterId = 3, MovieId = 4 },
                new MovieCharacter { ActorId = 3, CharacterId = 3, MovieId = 5 },
                new MovieCharacter { ActorId = 3, CharacterId = 3, MovieId = 6 },
                new MovieCharacter { ActorId = 3, CharacterId = 3, MovieId = 7 },
                new MovieCharacter { ActorId = 3, CharacterId = 3, MovieId = 8 },
                new MovieCharacter { ActorId = 4, CharacterId = 4, MovieId = 1 },
                new MovieCharacter { ActorId = 4, CharacterId = 4, MovieId = 2 },
                new MovieCharacter { ActorId = 5, CharacterId = 4, MovieId = 3 },
                new MovieCharacter { ActorId = 5, CharacterId = 4, MovieId = 4 },
                new MovieCharacter { ActorId = 5, CharacterId = 4, MovieId = 5 },
                new MovieCharacter { ActorId = 5, CharacterId = 4, MovieId = 6 },
                new MovieCharacter { ActorId = 5, CharacterId = 4, MovieId = 7 },
                new MovieCharacter { ActorId = 5, CharacterId = 4, MovieId = 8 },
                new MovieCharacter { ActorId = 6, CharacterId = 5, MovieId = 1 },
                new MovieCharacter { ActorId = 7, CharacterId = 5, MovieId = 4 },
                new MovieCharacter { ActorId = 7, CharacterId = 5, MovieId = 5 },
                new MovieCharacter { ActorId = 7, CharacterId = 5, MovieId = 7 },
                new MovieCharacter { ActorId = 7, CharacterId = 5, MovieId = 8 },
                new MovieCharacter { ActorId = 8, CharacterId = 6, MovieId = 9 },
                new MovieCharacter { ActorId = 8, CharacterId = 6, MovieId = 10 },
                new MovieCharacter { ActorId = 8, CharacterId = 6, MovieId = 11 },
                new MovieCharacter { ActorId = 9, CharacterId = 7, MovieId = 9 },
                new MovieCharacter { ActorId = 9, CharacterId = 7, MovieId = 10 },
                new MovieCharacter { ActorId = 9, CharacterId = 7, MovieId = 11 },
                new MovieCharacter { ActorId = 10, CharacterId = 8, MovieId = 9 },
                new MovieCharacter { ActorId = 10, CharacterId = 8, MovieId = 10 },
                new MovieCharacter { ActorId = 10, CharacterId = 8, MovieId = 11 },
                new MovieCharacter { ActorId = 11, CharacterId = 9, MovieId = 9 },
                new MovieCharacter { ActorId = 11, CharacterId = 9, MovieId = 10 },
                new MovieCharacter { ActorId = 11, CharacterId = 9, MovieId = 11 },
                new MovieCharacter { ActorId = 12, CharacterId = 10, MovieId = 9 },
                new MovieCharacter { ActorId = 13, CharacterId = 11, MovieId = 9 },
                new MovieCharacter { ActorId = 13, CharacterId = 11, MovieId = 10 },
                new MovieCharacter { ActorId = 13, CharacterId = 11, MovieId = 11 },
                new MovieCharacter { ActorId = 14, CharacterId = 12, MovieId = 9 },
                new MovieCharacter { ActorId = 14, CharacterId = 12, MovieId = 10 },
                new MovieCharacter { ActorId = 14, CharacterId = 12, MovieId = 11 },
                new MovieCharacter { ActorId = 15, CharacterId = 13, MovieId = 9 },
                new MovieCharacter { ActorId = 15, CharacterId = 13, MovieId = 10 },
                new MovieCharacter { ActorId = 15, CharacterId = 13, MovieId = 11 },
                new MovieCharacter { ActorId = 16, CharacterId = 14, MovieId = 9 },
                new MovieCharacter { ActorId = 16, CharacterId = 14, MovieId = 10 },
                new MovieCharacter { ActorId = 16, CharacterId = 14, MovieId = 11 }
            };

            // Add to database
            builder.Entity<Actor>().HasData(actors);
            builder.Entity<Character>().HasData(characters);
            builder.Entity<Director>().HasData(directors);
            builder.Entity<Franchise>().HasData(franchises);
            builder.Entity<Movie>().HasData(movies);
            builder.Entity<MovieCharacter>().HasData(movieCharacters);
        }
    }
}
