﻿using System;
using System.Collections.Generic;
using System.Text;

namespace task13.Models
{
    public class Actor : Person
    {
        public ICollection<MovieCharacter> Roles { get; set; }
    }
}
