﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace task13.Models
{
    public class Character
    {
        [Key]
        public int Id { get; set; }
        public string FullName { get; set; }
        public string Alias { get; set; }
        public string Gender { get; set; }
        public string PicURL { get; set; }

        // Relationships
        public ICollection<MovieCharacter> Roles { get; set; }
    }
}
