﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Text;

namespace task13.Models
{
    public class Movie
    {
        [Key]
        public int Id { get; set; }

        // Fields
        public string Title { get; set; }
        public string Genre { get; set; }
        public string Synopsis { get; set; }
        public DateTime? ReleaseYear { get; set; }
        public string PicURL { get; set; }
        public string TrailerURL { get; set; }

        // Relationships
        public ICollection<MovieCharacter> Cast { get; set; }
        public int? DirectorId { get; set; }
        public Director Director { get; set; }
        public int? FranchiseId { get; set; }
        public Franchise Franchise { get; set; }
    }
}
