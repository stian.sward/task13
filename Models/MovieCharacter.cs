﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace task13.Models
{
    public class MovieCharacter
    {
        [Key]
        public int ActorId { get; set; }
        [Key]
        public int CharacterId { get; set; }
        [Key]
        public int MovieId { get; set; }

        // Fields
        public string PicURL { get; set; }

        // Relationships
        public Actor Actor { get; set; }
        public Character Character { get; set; }
        public Movie Movie { get; set; }
    }
}
